<?php
/**
 * Calculate x to the power y using exponentiation by squaring algorithm
 *
 * @param int $x
 * @param int $y
 * @return int
 */
function power($x, $y){
    $result = 1;

    while($y > 0){
        if($y & 1){
            $result = multiply($result, $x);
        }
        $x = multiply($x, $x);
        $y = $y >> 1;
    }

    return $result;
}

/**
 * Calculate multiplication of x and y using binary multiplication algorithm
 *
 * @param int $x
 * @param int $y
 * @return int
 */
function multiply($x, $y){
    $result = 0;

    while($y > 0){
        if($y & 1){
            $result += $x;
        }
        $x = $x << 1;
        $y = $y >> 1;
    }

    return $result;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . '_exec.php';
run();