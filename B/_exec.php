<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'console-helper.php';

function run(){
    $x = getInputArg(1);
    $y = getInputArg(2);

    if(!($x || $y)){
        die(sprintf("Usage: php %s x y\n", getScriptName()));
    }

    echo sprintf('%1$s ^ %2$s = %3$s' . "\n", $x, $y, power($x, $y));
}
