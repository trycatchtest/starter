<?php
/**
 * Get positive integer argument from console input
 *
 * @param int $position
 * @param mixed $default
 * @return mixed
 */
function getInputArg($position = 1, $default = null)
{
    $arguments = $GLOBALS['argv'];

    if(array_key_exists($position, $arguments)){
        $argument = $arguments[$position];
        if(!is_numeric($argument)){
            die("Passed argument should be a positive integer. Non numeric argument given.\n");
        }

        $argument = (int) $argument;

        if($argument < 1){
            die("Passed argument should be a positive integer.\n");
        }

        return $argument;
    }

    return $default;
}

function getScriptName()
{
    $arguments = $GLOBALS['argv'];

    return $arguments[0];
}