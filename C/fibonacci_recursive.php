<?php
/**
 * Calculate Fibonacci sequence using recursive algorithm, starting from F(1)
 *
 * @param int $n Sequence length
 * @param array $sequence Array containing Fibonacci numbers
 * @return int
 */
function fibonacci($n, & $sequence = []){
    if(0 == $n) {
        return 0;
    }

    if(1 == $n) {
        $sequence[1] = 1;
        return 1;
    }

    if(array_key_exists($n, $sequence)) {
        return $sequence[$n];
    }

    $sequence[$n] = $result = fibonacci($n-2, $sequence) + fibonacci($n-1, $sequence);

    return $result;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . '_exec.php';
run();
