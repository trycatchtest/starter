<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'console-helper.php';

function run(){
    $n = getInputArg(1, 10);

    $sequence = [];

    fibonacci($n, $sequence);

    echo implode(', ', $sequence)."\n";
}
