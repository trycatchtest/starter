<?php
/**
 * Calculate Fibonacci sequence using iterative algorithm, starting from F(1)
 *
 * @param int $n Sequence length
 * @param array $sequence Array containing Fibonacci numbers
 */
function fibonacci($n, & $sequence = []){
    $sequence [0] = 0;
    $sequence [1] = 1;

    $position = 2;

    while($position <= $n){
        $sequence[$position] = $sequence[$position-2] + $sequence[$position-1];

        $position++;
    }

    unset($sequence[0]);
}

require_once __DIR__ . DIRECTORY_SEPARATOR . '_exec.php';
run();
