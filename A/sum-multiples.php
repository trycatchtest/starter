<?php
/**
 * Calculate sum of all multiples of positive integer in a given range
 *
 * @param int $a
 * @param int $upperRange
 * @return int
 */
function sumMultiplesInRange($a, $upperRange){

    $numberOfMultiples = (int) ($upperRange / $a);
    $biggestMultiple = $numberOfMultiples * $a;

    $start = $a;
    if($numberOfMultiples & 1){ //normalize to always process even number of multiples
        $start = 0;
        $numberOfMultiples++;
    }

    return ($biggestMultiple + $start) * ($numberOfMultiples >> 1);
}

/**
 * Calculate sum of all multiples for a pair of positive integers in a given range
 *
 * @param int $a
 * @param int $b
 * @param int $lcm
 * @param int $below
 * @return int
 */
function calculateSum($a, $b, $lcm, $below){
    $upperRange = $below - 1;

    $sumA = sumMultiplesInRange($a, $upperRange);
    $sumB = sumMultiplesInRange($b, $upperRange);
    $sumLcm = sumMultiplesInRange($lcm, $upperRange);

    return ($sumA + $sumB - $sumLcm);
}

function run($a, $b, $lcm, $rangeBelow){
    $sum = calculateSum($a, $b, $lcm, $rangeBelow);
    echo sprintf('The sum of all natural numbers below %1$s that are multiples of %2$s or %3$s is %4$s' . "\n", $rangeBelow, $a, $b, $sum);
}

$a = 3;
$b = 5;
$lcm = 15; //least common multiple of 3 and 5
$rangeBelow = 1000;
run($a, $b, $lcm, $rangeBelow);

$a = 3;
$b = 4;
$lcm = 12; //least common multiple of 3 and 4
$rangeBelow = 1000;
run($a, $b, $lcm, $rangeBelow);
